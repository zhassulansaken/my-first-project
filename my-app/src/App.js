import React, { useState } from 'react';
// import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';


function App() {
  const [count, setCount] = useState(0);

  function increment() {
    setCount(count + 1);
  }
  
  function decrement() {
    setCount(count - 1);
  }

  return (
    <div className="App">
      <button style={{backgroundColor: "red",color: "yellow"}} onClick={increment}><AddIcon/></button>
      <h1>{count}</h1>
      <button style={{backgroundColor: "yellow",color: "red"}} onClick={decrement}><RemoveIcon/></button>
    </div>
  );
}
export default App;